// Find the top 10 players with the highest probability of scoring a goal in a match

const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

const playersFilePath = path.resolve(__dirname, '../data/WorldCupPlayers.csv');

csvtojson().fromFile(playersFilePath).then((playersData) => {
    // console.log(playersData);
    const topTenPlayers = highestGoalScoring(playersData);
    // console.log(topTenPlayers);
    fs.writeFileSync('../public/output/4-ten-players-highest-probability-of-scoring-goal.json', JSON.stringify(topTenPlayers))
});

const highestGoalScoring = (playersData) => {
    const playerGoals = playersData.reduce((acc, player) => {
        if (player.Event.includes("G")) {
            const playerKey = `${player['Team Initials']}-${player['Player Name']}`;
            if (acc[playerKey]) {
                acc[playerKey]++;
            } else {
                acc[playerKey] = 1;
            }  // 10 ,15  ==> 10/10+15 
        }
        // console.log(acc);
        return acc;
    }, {});

    const totalGoals = Object.values(playerGoals).reduce((total, goals) => total + goals, 0);

    const topPlayers = Object.entries(playerGoals)
        .sort((a, b) => b[1] - a[1])
        .slice(0, 10)
        .map(([playerKey, goals]) => {
            const [teamInitials, playerName] = playerKey.split("-");
            const probability = goals / (totalGoals*(10/100));
            return { playerName, teamInitials, probability };
        });
    // console.log(topPlayers);
    return topPlayers;
};

module.exports = highestGoalScoring;






