

const { redCards, year2014, countRedCardsPerTeam } = require('./3-number-of-red-cards-issued-per-team-2014.cjs');


const path = require('path');
const csvtojson = require('csvtojson');
const fs = require('fs');

const WorldCupsFilePath = path.resolve(__dirname, '../data/WorldCupPlayers.csv');
const matchesFilePath = path.resolve(__dirname, '../data/WorldCupMatches.csv');
const outputFilePath = path.resolve(__dirname, '../public/output/3-number-of-red-cards-2014.json');
const matchesData2014Output = path.resolve(__dirname, '../public/output/3-1-matchesData-2014.json');
const redCardsDataOutput = path.resolve(__dirname, '../public/output/3-2-red-cards-data.json');

test("should return correct matches played per team", () => {
    csvtojson().fromFile(WorldCupsFilePath).then((players) => {
        csvtojson().fromFile(matchesFilePath).then((matches) => {
            const matchesData = year2014(matches);
            const redCardsData = redCards(players, matchesData);
            const result = countRedCardsPerTeam(redCardsData, matchesData);

            const expectedOutput = JSON.parse(fs.readFileSync(outputFilePath, 'utf8'));
            expect(result).toEqual(expectedOutput);

        })
    });
});


test('should handle missing or incorrect Matches file path', () => {
    const invalidFilePath = '../data/WorlCupsMatch.csv';

    return expect(csvtojson().fromFile(invalidFilePath)).rejects.toThrow();
});


test("should return correct matches played per team", () => {
    csvtojson().fromFile(WorldCupsFilePath).then((players) => {
        csvtojson().fromFile(matchesFilePath).then((matches) => {
            const matchesData = year2014(matches);
            const matchesExpectedOutput = JSON.parse(fs.readFileSync(matchesData2014Output, 'utf8'));
            expect(matchesData).toEqual(matchesExpectedOutput);
        })
    });
});



test('should handle an empty Matches file', (done) => {
    csvtojson().fromFile(matchesFilePath).then((matches) => {
        const players = [];
        const matchesData = year2014(matches);
        const redCardsData = redCards(players, matchesData);
        const result = countRedCardsPerTeam(redCardsData, matchesData);
        const expectedOutput = {}; // Update the expected output based on the structure you expect when there are no matches

        expect(result).toEqual(expectedOutput);
        done();
    }).catch((error) => {
        done(error);
    });
});


test('should handle an empty Matches file', (done) => {
    csvtojson().fromFile(matchesFilePath).then((matches) => {
        const emptyMatches = [];
        const matchesData = year2014(emptyMatches);
        const matchesExpectedOutput = [];

        expect(matchesData).toEqual(matchesExpectedOutput);
        done();
    }).catch((error) => {
        done(error);
    });
});

test('should handle an empty Matches file', (done) => {
    csvtojson().fromFile(WorldCupsFilePath).then((players) => {
        const emptyPlayers = [];
        const matchesData = redCards(emptyPlayers);
        const matchesExpectedOutput = [];

        expect(matchesData).toEqual(matchesExpectedOutput);
        done();
    }).catch((error) => {
        done(error);
    });
});




test("should return correct matches played per team", () => {
    csvtojson().fromFile(WorldCupsFilePath).then((players) => {
        csvtojson().fromFile(matchesFilePath).then((matches) => {
            const matchesData = year2014(matches);
            const redCardsData = redCards(players, matchesData);

            const redCardsExpectedOutput = JSON.parse(fs.readFileSync(redCardsDataOutput, 'utf8'));
            expect(redCardsData).toEqual(redCardsExpectedOutput);
        })

    });
});


