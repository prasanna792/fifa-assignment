// // Find the number of red cards issued per team in 2014 World Cup


const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

let playersFilePath = path.resolve(__dirname, '../data/WorldCupPlayers.csv');
let matchesFilePath = path.resolve(__dirname, '../data/WorldCupMatches.csv');

csvtojson().fromFile(playersFilePath).then((players) => {
    csvtojson().fromFile(matchesFilePath).then((matches) => {
        const matchesData = year2014(matches);
        fs.writeFileSync('../public/output/3-1-matchesData-2014.json', JSON.stringify(matchesData));
        const redCardsData = redCards(players,matchesData);
        fs.writeFileSync('../public/output/3-2-red-cards-data.json', JSON.stringify(redCardsData));

        const redCardsPerTeam = countRedCardsPerTeam(redCardsData, matchesData);

        // const finalResult = Object.entries(redCardsPerTeam);
          
        // console.log(finalResult);
        fs.writeFileSync('../public/output/3-number-of-red-cards-2014.json', JSON.stringify(redCardsPerTeam));
    });
});




const redCards = (players, matchesData) => {
    return players.filter((item) => (matchesData).includes(item.MatchID) && item.Event.includes('R'));
};

const year2014 = (matches) => {
    const matchIds = []
    matches.forEach(item => {
        if (item.Year.includes('2014')) {
            matchIds.push(item.MatchID)
        }
    });
    return matchIds
};

const countRedCardsPerTeam = (redCardsData, matchesData) => {
    // console.log(redCardsData.length);
    const redCardsPerTeam = {};

    redCardsData.forEach((card) => {
        if (redCardsPerTeam[card['Team Initials']]) {
            redCardsPerTeam[card['Team Initials']]++;
        } else {
            redCardsPerTeam[card['Team Initials']] = 1;
        }
    });

    return redCardsPerTeam;
};

module.exports = {
    redCards,
    year2014,
    countRedCardsPerTeam,
}






// const redCards = (players) => {
//     return players.filter((item) => item.Event.includes('R'));
// };

// const year2014 = (matches) => {
//     return matches.filter((item) => item.Year.includes('2014'));
// };

// const countRedCardsPerTeam = (redCardsData, matchesData) => {
//     const redCardsPerTeam = {};

//     redCardsData.forEach((card) => {
//         // console.log(card);
//         const teamInitials = card['Team Initials'];
//         const match = matchesData.find(
//             (item) => {
//                 if(item['Home Team Initials'] === teamInitials){
//                     console.log(item);
//                     return true
//                 }
//             }
//         );
//         // console.log(match);
//         if (match) {
//             if (redCardsPerTeam[teamInitials]) {
//                 redCardsPerTeam[teamInitials]++;
//             } else {
//                 redCardsPerTeam[teamInitials] = 1;
//             }
//         }
//     });
    

//     return redCardsPerTeam;
// };
