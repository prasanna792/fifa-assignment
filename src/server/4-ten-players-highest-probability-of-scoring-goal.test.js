const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

const highestGoalScoring = require('./4-ten-players-highest-probability-of-scoring-goal.cjs');

const playersFilePath = path.resolve(__dirname, '../data/WorldCupPlayers.csv');
const outputFilePath = path.resolve(__dirname,'../public/output/4-ten-players-highest-probability-of-scoring-goal.json');




test("should return correct matches played per team", () => {
    csvtojson().fromFile(playersFilePath).then((playersData) => {
        const result = highestGoalScoring(playersData);
        const expectedOutput = JSON.parse(fs.readFileSync(outputFilePath, 'utf8'));

        expect(result).toEqual(expectedOutput)
    });
});

test('should handle players data with no goal events', (done) => {
    const playersData = [
      { 'Team Initials': 'ARG', 'Player Name': 'Lionel Messi', Event: 'A' },
      { 'Team Initials': 'POR', 'Player Name': 'Cristiano Ronaldo', Event: 'F' },
    ];
  
    const result = highestGoalScoring(playersData);
    const expectedOutput = [];
  
    expect(result).toEqual(expectedOutput);
    done();
  });
  

  test('should handle players data with multiple goals by the same player', (done) => {
    const playersData = [
      { 'Team Initials': 'ARG', 'Player Name': 'Lionel Messi', Event: 'G' },
      { 'Team Initials': 'POR', 'Player Name': 'Cristiano Ronaldo', Event: 'G' },
      { 'Team Initials': 'POR', 'Player Name': 'Cristiano Ronaldo', Event: 'G' },
      
    ];
  
    const result = highestGoalScoring(playersData);
    const expectedOutput = [
      { playerName: 'Cristiano Ronaldo', teamInitials: 'POR', probability: expect.any(Number) },
      { playerName: 'Lionel Messi', teamInitials: 'ARG', probability: expect.any(Number) },

    ];
  
    expect(result).toEqual(expectedOutput);
    done();
  });
  









  