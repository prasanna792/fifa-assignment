// Number of matches played per city

const fs = require('fs');
const csvtojson = require('csvtojson');
const path= require('path');

let matchesFilePath = path.resolve(__dirname,'../data/WorldCupMatches.csv');

csvtojson().fromFile(matchesFilePath).then((matchesFile) => {
    // console.log(matchesFile[0].City);
    let resultMatchesPlayedPerCity = matchesPlayedPerCity(matchesFile)
    // console.log(resultMatchesPlayedPerCity);
    fs.writeFileSync('../public/output/1-matches-played-per-city.json',JSON.stringify(resultMatchesPlayedPerCity));
})

const matchesPlayedPerCity = (matchesFile) => {
  if (!matchesFile) {
    return {}; // Return an empty object when matchesFile is not provided
  }

  const matchesPerCityResult = matchesFile.reduce((accumulator, currentValue) => {
    const city = currentValue.City;
    if (city) {
      if (accumulator[city]) {
        accumulator[city] += 1;
      } else {
        accumulator[city] = 1;
      }
    }
    return accumulator;
  }, {});

  return matchesPerCityResult;
};

module.exports = matchesPlayedPerCity;


















