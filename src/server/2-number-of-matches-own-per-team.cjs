const fs = require('fs');
const csvtojson = require('csvtojson');
const path = require('path');

const matchesWonPerTeam = (matches) => {
    if (!matches || matches.length === 0) {
        return {};
    }

    const result = matches.reduce((accumulator, currentValue) => {
        if (currentValue.Winner) {
            if (accumulator[currentValue.Winner]) {
                accumulator[currentValue.Winner] += 1;
            } else {
                accumulator[currentValue.Winner] = 1;
            }
        }
        return accumulator;
    }, {});
    return result;
};


let worldCupsFilePath = path.resolve(__dirname, '../data/WorldCups.csv');

csvtojson().fromFile(worldCupsFilePath).then((matchesFile) => {
    const problem2 = matchesWonPerTeam(matchesFile);
    fs.writeFileSync('../public/output/2-number-of-matches-own-per-team.json', JSON.stringify(problem2));
});

module.exports = matchesWonPerTeam;
