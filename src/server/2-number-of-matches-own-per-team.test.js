const matchesWonPerTeam = require('./2-number-of-matches-own-per-team');
const path = require('path');
const csvtojson = require('csvtojson');
const fs = require('fs');

const csvFilePath = path.resolve(__dirname, '../data/WorldCups.csv');
const outputFilePath = path.resolve(__dirname, '../public/output/2-number-of-matches-own-per-team.json');




test("should return correct matches played per team", () => {
    csvtojson().fromFile(csvFilePath).then((matchesFile) => {
        const result = matchesWonPerTeam(matchesFile);
        const expectedOutput = JSON.parse(fs.readFileSync(outputFilePath, 'utf8'));
        expect(result).toEqual(expectedOutput);
    });
});

test("should return an empty object when no matches are provided", () => {
    const result = matchesWonPerTeam([]);
    expect(result).toEqual({});
});

test("should return an empty object when matchesFile is not provided", () => {
    const result = matchesWonPerTeam();
    expect(result).toEqual({});
});


test("should return correct matches won per team", () => {
    const matchesFile = [
      { "Home Team Name": "Team A", Winner: "Team A" },
      { "Home Team Name": "Team B", Winner: "Team B" },
      { "Home Team Name": "Team B", Winner: "Team A" },
    ];
    const expectedOutput = {
      "Team A": 2,
      "Team B": 1,
    
    };
  
    const result = matchesWonPerTeam(matchesFile);
  
    expect(result).toEqual(expectedOutput);
  });
  

  test("should handle matches with missing Winnner information", () => {
    const matchesFile = [
        { "Home Team Name": "Team A", Winner: "" },
        { "Home Team Name": "Team B", Winner: "Team B" },
        { "Home Team Name": "Team B", Winner: "Team A" },
      ];
    const expectedOutput = {
        "Team A": 1,
        "Team B": 1,
    };
   
    const result =  matchesWonPerTeam(matchesFile);
  
    expect(result).toEqual(expectedOutput);
  });
  