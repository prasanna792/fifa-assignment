


const matchesPlayedPerCity = require('./1-matches-played-per-city.cjs');
const output = require('../public/output/1-matches-played-per-city.json');
const path = require('path');
const csvtojson = require('csvtojson');

const csvFilePath = path.resolve(__dirname, '../data/WorldCupMatches.csv');

test("should return correct matches played per city", () => {
    csvtojson().fromFile(csvFilePath).then((matchesFile) => {
        const result = matchesPlayedPerCity(matchesFile);
        expect(result).toEqual(output);
        
    });
});




test("should return an empty object when no matches are provided", () => {
    const result = matchesPlayedPerCity([]);
    expect(result).toEqual({});
});

test("should return an empty object when matchesFile is not provided", () => {
    const result = matchesPlayedPerCity();
    expect(result).toEqual({});
});


test("should return correct matches played per city when there are matches in multiple cities", () => {
    const matchesFile = [
      { City: "London" },
      { City: "London" },
      { City: "Paris" },
      { City: "Paris" },
      { City: "Paris" },
      { City: "Berlin" },
    ];
    const expectedOutput = {
      London: 2,
      Paris: 3,
      Berlin: 1,
    };
  
    const result = matchesPlayedPerCity(matchesFile);
  
    expect(result).toEqual(expectedOutput);
  });
  
  test("should return an empty object when there are no matches", () => {
    const matchesFile = [];
    const expectedOutput = {};
  
    const result = matchesPlayedPerCity(matchesFile);
  
    expect(result).toEqual(expectedOutput);
  });
  
  test("should handle matches with missing city information", () => {
    const matchesFile = [
      { City: "London" },
      { City: "" },
      { City: "Paris" },
      { City: "Berlin" },
    ];
    const expectedOutput = {
      London: 1,
      Paris: 1,
      Berlin: 1,
    };
  
    const result = matchesPlayedPerCity(matchesFile);
  
    expect(result).toEqual(expectedOutput);
  });
  
